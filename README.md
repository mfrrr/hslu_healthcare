# Risk Factors of Heart Diseases

This repository contains an analysis about the variable importance of risk factor variables in the prediction of heart diseases. The analysis was conducted using R and Python. 

Used libraries are among others:
* Pandas & Numpy (Python, data analysis)
* Seaborn & Matplotlib (Python, data visualization)
* Scikit-learn (Python, machine learning)
* Ggplot2 (R, data visualization)
* Mice (R, data imputation)
* Dominanceanalysis (R, variable dominance analysis)
